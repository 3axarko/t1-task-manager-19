package ru.t1.zkovalenko.tm.model;

import ru.t1.zkovalenko.tm.api.model.IWBS;
import ru.t1.zkovalenko.tm.enumerated.Status;

import java.util.Date;
import java.util.UUID;

public abstract class AbstractProjectTask extends AbstractModel implements IWBS {

    private String id = UUID.randomUUID().toString();

    private String name = "";

    private String description = "";

    private Status status = Status.NOT_STARTED;

    private Date created = new Date();

    public AbstractProjectTask() {
    }

    public AbstractProjectTask(String name) {
        this.name = name;
    }

    public AbstractProjectTask(String name, Status status) {
        this.name = name;
        this.status = status;
    }

    public AbstractProjectTask(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return name + " : " + description;
    }

    @Override
    public Date getCreated() {
        return created;
    }

    @Override
    public void setCreated(Date created) {
        this.created = created;
    }

}
