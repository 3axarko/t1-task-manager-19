package ru.t1.zkovalenko.tm.model;

import ru.t1.zkovalenko.tm.enumerated.Status;

public final class Task extends AbstractProjectTask {

    private String projectId;

    public Task() {
    }

    public Task(String name) {
        super(name);
    }

    public Task(String name, Status status) {
        super(name, status);
    }

    public Task(String name, String description) {
        super(name, description);
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

}
