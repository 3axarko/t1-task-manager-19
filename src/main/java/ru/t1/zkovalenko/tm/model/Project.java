package ru.t1.zkovalenko.tm.model;

import ru.t1.zkovalenko.tm.enumerated.Status;

public final class Project extends AbstractProjectTask {

    public Project() {
    }

    public Project(String name) {
        super(name);
    }

    public Project(String name, Status status) {
        super(name, status);
    }

    public Project(String name, String description) {
        super(name, description);
    }

}
