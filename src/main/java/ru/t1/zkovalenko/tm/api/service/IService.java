package ru.t1.zkovalenko.tm.api.service;

import ru.t1.zkovalenko.tm.api.repository.IRepository;
import ru.t1.zkovalenko.tm.enumerated.Sort;
import ru.t1.zkovalenko.tm.model.AbstractModel;

import java.util.Comparator;
import java.util.List;

public interface IService<M extends AbstractModel> extends IRepository<M> {

    @Override
    void clear();

    @Override
    List<M> findAll();

    @Override
    List<M> findAll(Comparator comparator);

    List<M> findAll(Sort sort);

    @Override
    M add(M project);

    @Override
    boolean existById(String id);

    @Override
    M findOneById(String id);

    @Override
    M findOneByIndex(Integer index);

    @Override
    int getSize();

    M remove(M model);

    @Override
    M removeById(String id);

    @Override
    M removeByIndex(Integer index);
}
