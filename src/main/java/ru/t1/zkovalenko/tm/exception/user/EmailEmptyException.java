package ru.t1.zkovalenko.tm.exception.user;

import ru.t1.zkovalenko.tm.exception.AbstractException;

public final class EmailEmptyException extends AbstractUserException {

    public EmailEmptyException() {
        super("Email is empty");
    }

}
